<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/formidable/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// F
	'anonymiser_revisions_description' => 'Anonymiser les révisions plus anciennes que _CNIL_PERIODE en hashant les adresse IP',
	'anonymiser_revisions_slogan' => 'Stop surveillance !'
);
